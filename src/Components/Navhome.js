import React from 'react';
import { NavLink } from "react-router-dom";

const Navhome = () => {
    return (
        <div class="btn btn-primary px-2 my-2">
            <NavLink to="/">
                <button className='btn btn-primary'>Home</button>
            </NavLink>
        </div>
        
    );
};

export default Navhome;