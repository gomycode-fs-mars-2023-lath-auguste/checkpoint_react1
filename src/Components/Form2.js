import React from 'react';

const Form2 = () => {
    return (
        <div>
            <form>
                {/* <!-- Name input --> */}
                <div className="form-outline mb-4">
                    <label className="form-label" for="form1Example1">Name</label>
                    <input type="name" id="form1Example1" className="form-control" placeholder='Alex Doe'/>
                </div>

                {/* <!-- Email input --> */}
                <div className="form-outline mb-4">
                    <label className="form-label" for="form1Example1">Email address</label>
                    <input type="email" id="form1Example1" className="form-control" placeholder='youremail@domaine.com'/>
                </div>

                {/* <!-- Password input --> */}
                <div className="form-outline mb-4">
                    <label className="form-label" for="form1Example2">Password</label>
                    <input type="password" id="form1Example2" className="form-control" placeholder='......' />
                </div>

                {/* <!-- Submit button --> */}
                <button type="submit" className="btn btn-primary btn-block">Sign Up</button>
            </form>
        </div>
    );
};

export default Form2;