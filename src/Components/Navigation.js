import React from 'react';
import { NavLink } from "react-router-dom";

const Navigation = () => {
    return (
        <div class="d-grid gap-2" role="group" aria-label="Vertical button group">
            <NavLink to="/login">
                <button className='btn btn-primary fs-4 px-2 m-2 w-50'>Login</button>
            </NavLink>
            <NavLink to="/signup">
                <button className='btn btn-primary fs-4 px-2 m-2 w-50'>Sign Up</button>
            </NavLink>
        </div>
        
    );
};

export default Navigation;