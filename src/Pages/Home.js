import React from 'react';
import Navigation from '../Components/Navigation';

const Home = () => {
    return (
        <div className='py-5'>
            <div className='container py-5 text-center '>
                <h1 className='display-3 fw-bold pb-5'>Welcome</h1>
                <Navigation className='d-flex'/>
            </div>
        </div>
    );
};

export default Home;