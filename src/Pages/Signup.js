import React from 'react';
import Form2 from '../Components/Form2';
import Navhome from '../Components/Navhome';

const Signup = () => {
    return (
        <div className='py-5'>
            <div className="container text-center w-50 py-5">
                <Form2/>
                <Navhome/>
            </div>
        </div>
    );
};

export default Signup;