import React from 'react';
import Form from '../Components/Form';
import Navhome from '../Components/Navhome';

const Login = () => {
    return (
        <div className='py-5'>
            <div className="container text-center w-50 py-5">
                <Form/>
                <Navhome/>
            </div>
        </div>
    );
};

export default Login;